package nl.ing.beleggen.stocks.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Stock {

    @NonNull
    private String isin;

    @NonNull
    private String name;

    @NonNull
    private Amount currentPrice;

    private LocalDateTime lastUpdate;

    public void updatePrice(final Amount amount) {
        Assert.notNull(amount, "New price must be defined to update old price");
        PriceValidator.validate(amount, currentPrice);
        currentPrice = amount;
        lastUpdate = LocalDateTime.now();
    }

    @Builder
    private Stock(final String isin,
                  final String name,
                  final BigDecimal price,
                  final String currency,
                  final LocalDateTime lastUpdate
    ) {
        setIsin(isin);
        setName(name);
        setCurrentPrice(Amount.builder()
                .value(price)
                .currency(currency)
                .build());
        setLastUpdate(lastUpdate);
    }
}
