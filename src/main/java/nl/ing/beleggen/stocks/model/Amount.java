package nl.ing.beleggen.stocks.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;

@Data
public class Amount {

    @NonNull
    private BigDecimal value;

    // TODO: gebruik enum of Currency
    @NonNull
    private String currency;

    @Builder
    private Amount(final BigDecimal value, final String currency) {
        setValue(value);
        setCurrency(currency);
    }
}
