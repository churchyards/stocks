package nl.ing.beleggen.stocks.model;

import org.springframework.util.Assert;

public class PriceValidator {

    public static boolean validate(final Amount newPrice, final Amount oldPrice) {
        Assert.notNull(newPrice, "New price must be defined");
        Assert.notNull(oldPrice, "Old price must be defined");

        if (newPrice.getValue().equals(oldPrice.getValue()) &&
                newPrice.getCurrency().equalsIgnoreCase(oldPrice.getCurrency())) {
            throw new PriceInvalidException(String.format("New price should differ from old price: %s %s",
                    oldPrice.getValue(), oldPrice.getCurrency()));
        }
        return true;
    }

}
