package nl.ing.beleggen.stocks.model;

public class PriceInvalidException extends RuntimeException {

    public PriceInvalidException(final String message) {
        super(message);
    }
}
