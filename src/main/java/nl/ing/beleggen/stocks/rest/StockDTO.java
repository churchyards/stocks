package nl.ing.beleggen.stocks.rest;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import nl.ing.beleggen.stocks.model.Stock;

import java.time.LocalDateTime;

@Data
public class StockDTO {

    @NonNull
    private String isin;

    @NonNull
    private String name;

    @NonNull
    private AmountDTO currentPrice;

    private LocalDateTime lastUpdate;

    public static StockDTO fromDomain(final Stock stock) {
        return StockDTO.builder().stock(stock).build();
    }

    @Builder
    private StockDTO(final Stock stock) {
        setIsin(stock.getIsin());
        setName(stock.getName());
        setCurrentPrice( AmountDTO.builder().amount(stock.getCurrentPrice()).build() );
        setLastUpdate(stock.getLastUpdate());
    }

}
