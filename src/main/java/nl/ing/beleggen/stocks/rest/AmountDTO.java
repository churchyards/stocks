package nl.ing.beleggen.stocks.rest;

import lombok.Builder;
import lombok.Data;
import nl.ing.beleggen.stocks.model.Amount;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class AmountDTO {

    @NotNull(message = "Value is required")
    @DecimalMin(value = "0", message = "Value must be greater or equal to 0")
    private BigDecimal value;

    // TODO: gebruik enum of Currency
    @NotNull(message = "Currency is required")
    private String currency;

    public AmountDTO() {
    }

    @Builder
    private AmountDTO(final Amount amount) {
        setValue(amount.getValue());
        setCurrency(amount.getCurrency());
    }
}
