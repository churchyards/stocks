package nl.ing.beleggen.stocks.rest;

import lombok.Data;
import lombok.NonNull;

@Data
public class PriceUpdateDTO {

    @NonNull
    private AmountDTO newPrice;

}
