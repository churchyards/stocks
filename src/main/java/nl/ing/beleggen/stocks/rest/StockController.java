package nl.ing.beleggen.stocks.rest;

import nl.ing.beleggen.stocks.model.Amount;
import nl.ing.beleggen.stocks.model.PriceInvalidException;
import nl.ing.beleggen.stocks.model.Stock;
import nl.ing.beleggen.stocks.service.StockNotFoundException;
import nl.ing.beleggen.stocks.service.StockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/stocks", produces = MediaType.APPLICATION_JSON_VALUE)
public class StockController {

    private final Logger logger = LoggerFactory.getLogger(StockController.class);

    private final StockService stockService;

    @Autowired
    public StockController(final StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping()
    public List<StockDTO> getAllStocks() {
        try {
            return stockService.getAllStocks().stream()
                    .map( obj -> StockDTO.fromDomain(obj) )
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw toResponseStatusException(e);
        }
    }

    @GetMapping(path = "/{isin}")
    public StockDTO getStockFor(@PathVariable(name = "isin") final String isin) {
        try {
            return StockDTO.fromDomain(stockService.getStockFor(isin));
        } catch (Exception e) {
            throw toResponseStatusException(e);
        }
    }

    @PatchMapping(path = "/{isin}")
    public StockDTO updatePriceFor(
            @PathVariable(name = "isin") final String isin,
            @Valid @RequestBody PriceUpdateDTO priceUpdateDTO
    ) {
        try {
            final Stock stock = stockService.getStockFor(isin);
            stock.updatePrice(Amount.builder()
                    .value(priceUpdateDTO.getNewPrice().getValue())
                    .currency(priceUpdateDTO.getNewPrice().getCurrency())
                    .build());
            return StockDTO.fromDomain(stockService.save(stock));
        } catch (Exception e) {
            throw toResponseStatusException(e);
        }
    }

    private ResponseStatusException toResponseStatusException(final Exception e) {
        if (e instanceof StockNotFoundException) {
            return new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        if (e instanceof PriceInvalidException) {
            return new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        logger.error("Unexpected error", e);
        return new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, e.getMessage());
    }
}
