package nl.ing.beleggen.stocks.service;

public class StockNotFoundException extends RuntimeException {

    public StockNotFoundException(final String message) {
        super(message);
    }
}
