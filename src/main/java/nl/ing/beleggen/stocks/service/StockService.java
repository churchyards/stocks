package nl.ing.beleggen.stocks.service;

import nl.ing.beleggen.stocks.infrastructure.jpa.StockEntity;
import nl.ing.beleggen.stocks.infrastructure.jpa.StockRepository;
import nl.ing.beleggen.stocks.model.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class StockService {

    private final StockRepository repository;

    @Autowired
    public StockService(final StockRepository repository) {
        this.repository = repository;
    }

    public List<Stock> getAllStocks() {
        return repository.findAll().stream()
                .map( entity -> entity.toDomain() )
                .collect(Collectors.toList());
    }

    public Stock getStockFor(final String isin) {
        return getStockEntityFor(isin).toDomain();
    }

    public Stock save(final Stock stock) {
        final StockEntity entity = getStockEntityFor(stock.getIsin());
        entity.setPrice(stock.getCurrentPrice().getValue());
        entity.setCurrency(stock.getCurrentPrice().getCurrency());
        entity.setLastUpdate(stock.getLastUpdate());
        return repository.save(entity).toDomain();
    }

    private StockEntity getStockEntityFor(final String isin) {
        return repository.findStockEntityByIsin(isin)
                .orElseThrow( () -> new StockNotFoundException(String.format("Could not find stock for ISIN: '%s'", isin)) );
    }
}
