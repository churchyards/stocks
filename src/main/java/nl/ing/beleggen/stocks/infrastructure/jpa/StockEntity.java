package nl.ing.beleggen.stocks.infrastructure.jpa;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.ing.beleggen.stocks.model.Stock;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "STOCKS")
@Data
@NoArgsConstructor
public class StockEntity {

    @Id
    private Long id;

    @NonNull
    private String isin;

    @NonNull
    private String name;

    @NonNull
    private BigDecimal price;

    @NonNull
    private String currency;

    @Column(name = "UPDATED_AT")
    private LocalDateTime lastUpdate;

    public Stock toDomain() {
        return Stock.builder()
                .isin(isin)
                .name(name)
                .price(price)
                .currency(currency)
                .lastUpdate(lastUpdate)
                .build();
    }
}
