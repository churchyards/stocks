DROP TABLE IF EXISTS stocks;

CREATE TABLE stocks (
  id INT AUTO_INCREMENT PRIMARY KEY,
  isin VARCHAR(12) NOT NULL UNIQUE,
  name VARCHAR(250) NOT NULL,
  price NUMBER NOT NULL,
  currency VARCHAR(3) NOT NULL,
  updated_at TIMESTAMP DEFAULT NULL
);

INSERT INTO stocks (isin, name, price, currency) VALUES
  ('BE0003788057', 'Telindus', 10, 'EUR'),
  ('US0378331005', 'Apple', 100, 'EUR'),
  ('DE0007100000', 'DaimlerChrysler AG', 30, 'EUR'),
  ('GB0002634946', 'BAE Systems', 15, 'EUR');


