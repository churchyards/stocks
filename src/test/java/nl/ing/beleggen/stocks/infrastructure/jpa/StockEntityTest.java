package nl.ing.beleggen.stocks.infrastructure.jpa;

import nl.ing.beleggen.stocks.model.Stock;
import org.junit.Assert;
import org.junit.Test;

public class StockEntityTest {

    @Test
    public void toDomain() {
        final Stock model = StockEntityObjectMother.notUpdatedAppleStockEntity().toDomain();

        Assert.assertEquals(StockEntityObjectMother.APPLE_ISIN, model.getIsin());
        Assert.assertEquals(StockEntityObjectMother.APPLE_NAME, model.getName());
        Assert.assertEquals(StockEntityObjectMother.APPLE_PRICE, model.getCurrentPrice().getValue());
        Assert.assertEquals(StockEntityObjectMother.EURO_CURRENCY, model.getCurrentPrice().getCurrency());
        Assert.assertNull(model.getLastUpdate());
    }
}
