package nl.ing.beleggen.stocks.infrastructure.jpa;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class StockEntityObjectMother {

    public static final String APPLE_ISIN = "US0378331005";
    public static final String APPLE_NAME = "Apple";
    public static final BigDecimal APPLE_PRICE = new BigDecimal(100);
    public static final BigDecimal UPDATED_APPLE_PRICE = new BigDecimal(110);
    public static final String GOOGLE_ISIN = "US0378331006";
    public static final String GOOGLE_NAME = "Google";
    public static final BigDecimal GOOGLE_PRICE = new BigDecimal(150);
    public static final String EURO_CURRENCY = "EUR";

    public static StockEntity notUpdatedAppleStockEntity() {
        final StockEntity e = new StockEntity();
        e.setId(1L);
        e.setIsin(APPLE_ISIN);
        e.setName(APPLE_NAME);
        e.setPrice(APPLE_PRICE);
        e.setCurrency(EURO_CURRENCY);
        return e;
    }

    public static StockEntity updatedAppleStockEntity() {
        final StockEntity e = new StockEntity();
        e.setId(1L);
        e.setIsin(APPLE_ISIN);
        e.setName(APPLE_NAME);
        e.setPrice(UPDATED_APPLE_PRICE);
        e.setCurrency(EURO_CURRENCY);
        e.setLastUpdate(LocalDateTime.of(2019, 9, 19, 12, 0));
        return e;
    }

    public static StockEntity notUpdatedGoogleStockEntity() {
        final StockEntity e = new StockEntity();
        e.setId(2L);
        e.setIsin(GOOGLE_ISIN);
        e.setName(GOOGLE_NAME);
        e.setPrice(GOOGLE_PRICE);
        e.setCurrency(EURO_CURRENCY);
        return e;
    }

    public static List<StockEntity> stockEntities() {
        final List<StockEntity> list = new ArrayList<>();
        list.add(notUpdatedAppleStockEntity());
        list.add(notUpdatedGoogleStockEntity());
        return list;
    }
}
