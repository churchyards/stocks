package nl.ing.beleggen.stocks.model;

import org.junit.Assert;
import org.junit.Test;

public class StockTest {

    @Test
    public void updatePrice() {
        final Stock s = StockObjectMother.notUpdatedAppleStock();
        s.updatePrice(AmountObjectMother.euro10());

        Assert.assertEquals(s.getCurrentPrice(), AmountObjectMother.euro10());
        Assert.assertNotNull(s.getLastUpdate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void updatePriceNullInput() {
        final Stock s = StockObjectMother.notUpdatedAppleStock();
        s.updatePrice(null);

        Assert.assertEquals(s.getCurrentPrice(), AmountObjectMother.euro10());
        Assert.assertNotNull(s.getLastUpdate());
    }
}
