package nl.ing.beleggen.stocks.model;


import org.junit.Assert;
import org.junit.Test;

public class PriceValidatorTest {

    @Test
    public void validateDifferentAmounts() {
        final boolean b = PriceValidator.validate(AmountObjectMother.euro10(), AmountObjectMother.euro20());
        Assert.assertTrue(b);
    }

    @Test(expected = PriceInvalidException.class)
    public void validateSameAmounts() {
        PriceValidator.validate(AmountObjectMother.euro10(), AmountObjectMother.euro10());
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateNullInput() {
        PriceValidator.validate(null, AmountObjectMother.euro10());
    }
}
