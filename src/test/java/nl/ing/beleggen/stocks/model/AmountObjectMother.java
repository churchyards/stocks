package nl.ing.beleggen.stocks.model;

import nl.ing.beleggen.stocks.infrastructure.jpa.StockEntityObjectMother;

import java.math.BigDecimal;

public class AmountObjectMother {

    public static Amount euro10() {
        return Amount.builder().value(new BigDecimal(10)).currency(StockEntityObjectMother.EURO_CURRENCY).build();
    }

    public static Amount euro20() {
        return Amount.builder().value(new BigDecimal(20)).currency(StockEntityObjectMother.EURO_CURRENCY).build();
    }

    public static Amount usd10() {
        return Amount.builder().value(new BigDecimal(10)).currency("USD").build();
    }
}
