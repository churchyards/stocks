package nl.ing.beleggen.stocks.model;

import nl.ing.beleggen.stocks.infrastructure.jpa.StockEntityObjectMother;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class StockObjectMother {

    public static Stock notUpdatedAppleStock() {
        return Stock.builder()
                .isin(StockEntityObjectMother.APPLE_ISIN)
                .name(StockEntityObjectMother.APPLE_NAME)
                .currency(StockEntityObjectMother.EURO_CURRENCY)
                .price(StockEntityObjectMother.APPLE_PRICE)
                .build();
    }

    public static Stock updatedAppleStock() {
        return Stock.builder()
                .isin(StockEntityObjectMother.APPLE_ISIN)
                .name(StockEntityObjectMother.APPLE_NAME)
                .currency(StockEntityObjectMother.EURO_CURRENCY)
                .price(StockEntityObjectMother.UPDATED_APPLE_PRICE)
                .lastUpdate(LocalDateTime.of(2019, 9, 19, 12, 0))
                .build();
    }

    public static Stock notUpdatedGoogleStock() {
        return Stock.builder()
                .isin(StockEntityObjectMother.GOOGLE_ISIN)
                .name(StockEntityObjectMother.GOOGLE_NAME)
                .currency(StockEntityObjectMother.EURO_CURRENCY)
                .price(StockEntityObjectMother.GOOGLE_PRICE)
                .build();
    }

    public static List<Stock> stocks() {
        final List<Stock> list = new ArrayList<>();
        list.add(notUpdatedAppleStock());
        list.add(notUpdatedGoogleStock());
        return list;
    }
}
