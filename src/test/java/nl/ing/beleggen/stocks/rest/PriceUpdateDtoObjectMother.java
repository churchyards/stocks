package nl.ing.beleggen.stocks.rest;

import nl.ing.beleggen.stocks.model.AmountObjectMother;

public class PriceUpdateDtoObjectMother {

    public static PriceUpdateDTO euro10() {
        return new PriceUpdateDTO(AmountDTO.builder().amount(AmountObjectMother.euro10()).build());
    }

}
