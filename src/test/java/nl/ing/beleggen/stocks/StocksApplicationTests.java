package nl.ing.beleggen.stocks;

import nl.ing.beleggen.stocks.infrastructure.jpa.StockEntityObjectMother;
import nl.ing.beleggen.stocks.rest.PriceUpdateDtoObjectMother;
import nl.ing.beleggen.stocks.rest.StockController;
import nl.ing.beleggen.stocks.rest.StockDTO;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StocksApplicationTests {

	@Autowired
	private StockController stockController;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void findAllStocks() {
		List<StockDTO> list = stockController.getAllStocks();
		Assert.assertEquals(4, list.size());
	}

	@Test
	public void findSingleStock() {
		StockDTO found = stockController.getStockFor(StockEntityObjectMother.APPLE_ISIN);
		Assert.assertEquals(StockEntityObjectMother.APPLE_ISIN, found.getIsin());
	}

	@Test
	public void updatePrice() {
		StockDTO updated = stockController.updatePriceFor(
				StockEntityObjectMother.APPLE_ISIN,
				PriceUpdateDtoObjectMother.euro10()
		);
		Assert.assertNotNull(updated.getLastUpdate());
	}

}
