package nl.ing.beleggen.stocks.service;

import nl.ing.beleggen.stocks.infrastructure.jpa.StockEntityObjectMother;
import nl.ing.beleggen.stocks.infrastructure.jpa.StockRepository;
import nl.ing.beleggen.stocks.model.Stock;
import nl.ing.beleggen.stocks.model.StockObjectMother;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(value = MockitoJUnitRunner.class)
public class StockServiceTest {

    @Mock
    private StockRepository stockRepository;

    @InjectMocks
    private StockService stockService;

    @Test
    public void getAllStocks() {
        when(stockRepository.findAll()).thenReturn(StockEntityObjectMother.stockEntities());
        final List<Stock> list = stockService.getAllStocks();

        Assert.assertEquals(StockObjectMother.stocks(), list);
    }

    @Test
    public void getStockFor() {
        when(stockRepository.findStockEntityByIsin(StockEntityObjectMother.GOOGLE_ISIN))
                .thenReturn(Optional.of(StockEntityObjectMother.notUpdatedGoogleStockEntity()));
        final Stock found = stockService.getStockFor(StockEntityObjectMother.GOOGLE_ISIN);

        Assert.assertEquals(StockObjectMother.notUpdatedGoogleStock(), found);
    }

    @Test(expected = StockNotFoundException.class)
    public void getStockForNotFound() {
        when(stockRepository.findStockEntityByIsin(StockEntityObjectMother.GOOGLE_ISIN))
                .thenReturn(Optional.empty());
        stockService.getStockFor(StockEntityObjectMother.GOOGLE_ISIN);
    }

    @Test
    public void savePriceUpdate() {
        when(stockRepository.findStockEntityByIsin(StockEntityObjectMother.APPLE_ISIN))
                .thenReturn(Optional.of(StockEntityObjectMother.notUpdatedAppleStockEntity()));
        when(stockRepository.save(StockEntityObjectMother.updatedAppleStockEntity()))
                .thenReturn(StockEntityObjectMother.updatedAppleStockEntity());
        final Stock saved = stockService.save(StockObjectMother.updatedAppleStock());
        Assert.assertEquals(saved, StockObjectMother.updatedAppleStock());
    }
}
